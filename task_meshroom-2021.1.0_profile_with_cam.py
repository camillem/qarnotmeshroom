#!/usr/bin/env python3

# Taken from https://blog.qarnot.com/meshroom-on-qarnot-documentation/
# Import the Qarnot SDK
import qarnot
import mysecrets


# Create a connection, from which all other objects will be derived
# Enter client token here
QARNOT_TOKEN = mysecrets.MY_QARNOT_TOKEN
conn = qarnot.connection.Connection(client_token=QARNOT_TOKEN)

# Create the task
task = conn.create_task("PR avec Camera publish node fix", "meshroom-2021.1.0", 1)

# Create the input bucket and synchronize with a local folder
# Insert a local folder directory
input_bucket = conn.create_bucket("meshroom-in-Cam")
input_bucket.sync_directory("dataset")

# Attach the bucket to the task
task.resources.append(input_bucket)

# Create a result bucket and attach it to the task
task.results = conn.create_bucket("meshroom-out")

# Specify the task's parameters
task.constants["INPUT_FOLDER"] = "imgs_col_pr"
task.constants["OUTPUT_FOLDER"] = "output-folder"
task.constants["LOGS_FOLDER"] = "logs"
task.constants['MESHROOM_EXTRA_FLAGS'] = "-p /job/AvecExportCam.mg"

# Submit the task
task.run(output_dir="output")
