# Qarnot & Meshroom

Tests de [Meshroom](https://github.com/alicevision/meshroom) sur [Qarnot](https://qarnot.com) 

## Éléments de référence

- La [documentation du SDK Python](https://qarnot.com/documentation/sdk-python/)
- Le [tutoriel générique](https://qarnot.com/fr/developers/get-started/python-tutorial)
- Le [nouveau tutoriel spécifique à Meshroom](https://blog.qarnot.com/meshroom-on-qarnot-documentation/)
- Le [tutoriel précédent](https://blog.qarnot.com/create-your-own-3d-model-with-photogrammetry/)

