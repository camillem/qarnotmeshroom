#!/usr/bin/env python3
import qarnot
import mysecrets

# A simple script using Qarnot's Python API
# https://qarnot.com/documentation/sdk-python/


QARNOT_TOKEN = mysecrets.MY_QARNOT_TOKEN

# Create a connection, from which all other objects will be derived
conn = qarnot.connection.Connection(client_token=QARNOT_TOKEN)

# Create a new task
task = conn.create_task('Visage', 'meshroom', 1)

# Create a new input and output bucket
input_bucket = conn.retrieve_or_create_bucket("Visage-input")
output_bucket = conn.create_bucket("Visage-outpuput")

# Synchronize your local dataset with the input bucket
input_bucket.sync_directory("dataset")

# Append the buckets to the task
task.resources.append(input_bucket)
task.results = output_bucket

# Specify the dataset folder name
task.constants['INPUT_FOLDER'] = "photogrammetry"

# Submit the task to the API
task.submit()



